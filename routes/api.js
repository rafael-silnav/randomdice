var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/api/1d6');
});

router.get('/:dice', function(req, res, next) {
	var dice = req.params.dice;
	var error = verifyDice(dice) || "success";

	var response = { error: error, result: null }

	if (error == "success") {
		var nums = dice.split("d");
		var amount = parseInt(nums[0]);
		var sides = parseInt(nums[1]);

		var result = 0;
		for (var i = 0; i < amount; i++) {
			result += Math.floor((Math.random() * sides) + 1);
		}
	
		response.result = result;
	}

	res.send(JSON.stringify(response));

});

function verifyDice(diceString) {
	var count = 0;
	for (var i = diceString.length - 1; i >= 0; i--) {
		if (diceString[i] === 'd' || diceString[i] === 'D')
			count++;
	}
	
	if (count != 1)
		return "dice not well formed"

	var nums = diceString.split("d");

	if (nums.length != 2)
		return "dice not well formed"

	if (isNaN(parseInt(nums[0])) || !isFinite(nums[0]))
		return "dice not well formed"

	if (isNaN(parseInt(nums[1])) || !isFinite(nums[1]))
		return "dice not well formed"

	var amount = parseInt(nums[0]);
	var sides = parseInt(nums[1]);

	if (amount < 0 || amount > 1000)
		return "out of the limits";

	if (sides < 0 || sides > 1000)
		return "out of the limits";

	return undefined;
}

module.exports = router;
